<?php
get_header(); 
$args = array(
        'post_type' => 'automobiles',
        'posts_per_page' => -1,
        'post_status' => 'publish'
    )
$query = new WP_Query($args);
if ($query->have_posts() ) : 
    echo '<select>';
    while ( $query->have_posts() ) : $query->the_post();
            echo '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
    endwhile;
    echo '</select>';
    wp_reset_postdata();
endif;

get_footer(); 
?>