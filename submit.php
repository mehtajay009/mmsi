<?php
$message = '';
$is_error = false;
if ($_POST) {
	$names = $_POST['name'];
	$healthids = $_POST['healthid'];
	if($names){
		foreach ($names as $key => $value) {
			if($value == ''){
				$message = "Name field is blank";
				$is_error = true;
				break;
			}
			elseif(strlen($value) < 3){
				$message = "Name field required minimum 3 characters.";
				$is_error = true;
				break;
			}
		}
	}
	if($healthids && !$is_error){
		foreach ($healthids as $key => $value) {
			if($value == ''){
				$message = "Health id field is blank";
				$is_error = true;
				break;
			}
			elseif(strlen($value) != 5 || !ctype_digit($value)){
				$message = "Invalid format for healthid.";
				$is_error = true;
				break;
			}
		}
	}
}
else{
	$message = "Please enter form details";
}
if($is_error){
	echo $message;
}
else{
	echo "SUCCESS";
}